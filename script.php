<?php


$url='https://api.zp.ru/v1/vacancies?search_type=fullThrottle&period=today&limit=100&average_salary=true&categories_facets=true&highlight=true&state=1&explain=1&geo_id=826';

$min_len=0;
$strtolower=0;
$topcount=0;
if(isset($_GET['delete_short']))
	$_GET['delete_short']?$min_len=2:$min_len=0;

if(isset($_GET['strtolower']))
	$_GET['strtolower']?$strtolower=1:$strtolower=0;

if(isset($_GET['top']))
	(int)$_GET['top']?$topcount=(int)$_GET['top']:$topcount=0;

$file=file_get_contents($url);
$file=json_decode($file);
$offset=0;

while(count($file->vacancies))
{
	foreach ($file->vacancies as $key => $value) {
		#подготавиливаем текст удаляем скобки,запятые,точки и тд
		$main_title=$value->header;
		$main_title=str_replace(')', '', $main_title);
		$main_title=str_replace('(', '', $main_title);
		$main_title=str_replace(',', '', $main_title);
		$main_title=str_replace('"', '', $main_title);
		$main_title=str_replace('.', ' ', $main_title);
		$main_title=str_replace(' /', '', $main_title);
		$main_title=str_replace(' - ', '-', $main_title);
		#приведение к нижнему регистру(опционально)
		($strtolower)?$main_title=mb_strtolower($main_title):$main_title=$main_title;
		$main_title=trim($main_title);

		$main_title=explode(' ',$main_title);

		foreach ($main_title as $word) {
			if(iconv_strlen($word)>$min_len)
				if(isset($words_list[$word]))
					$words_list[$word]['count']+=1;
				else
					$words_list[$word]=array('count'=>1);

		}

		foreach ($value->rubrics as  $rubric) {
			if(isset($rubric_list[$rubric->title]))
				$rubric_list[$rubric->title]['count']+=1;
			else
				$rubric_list[$rubric->title]=array('count'=>1);
		}
	}

	$offset+=100;
	$url="https://api.zp.ru/v1/vacancies?search_type=fullThrottle&period=today&limit=100&average_salary=true&categories_facets=true&highlight=true&state=1&explain=1&geo_id=826&offset={$offset}";
	$file=file_get_contents($url);
	$file=json_decode($file);
}


uasort($rubric_list,'countsort');
uasort($words_list,'countsort');



#функция для сортировки 
function countsort($a, $b)
{
    if ($a['count'] == $b['count']) {
        return 0;
    }
    return ($a['count'] > $b['count']) ? -1 : 1;
}


function echoReportTable($array,$count=0,$title='')
{
	if($count)
		$array=array_slice($array,0,$count-1);

	if($title)
		echo "<h2>{$title}</h2>";

	echo "<table class='table'>";

	foreach ($array as $key => $value) {
		echo"<tr><td>{$key}</td><td>{$value['count']}</td></tr>";
	}
	echo "</table>";
}


